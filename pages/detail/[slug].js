import Head from 'next/head'
import DetailPage from '../../component/pages/Detail'
import { BASE_URL, IMAGE_URL } from '/utils/Base'

const Detail = ({ dataDetail }) => {
  return (
    <div>
      <Head>
        <title>Detail | Pokemon</title>
        <meta name="description" content="This page showing detail pokemon" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <DetailPage dataDetail={dataDetail} />
    </div>
  )
}

export async function getServerSideProps({ query }) {
  let dataDetail = null

  try {
    const res = await fetch(`${BASE_URL}pokemon/${query.slug}`)
    const data = await res.json()

    dataDetail = { ...data, image: `${IMAGE_URL}${data.id}.png`}
  } catch (err) {
    dataDetail = { error: { message: err.message || 'Something went wrong' } }
  }
  return { props: { dataDetail } }
}

export default Detail
