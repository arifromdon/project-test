import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import HomePage from '../component/pages/Home'
import { useRouter } from 'next/router'
import { BASE_URL, IMAGE_URL } from '/utils/Base'

const Home = ({ listData }) => {
  const router = useRouter()
  const [pages, setPages] = useState(1)

  useEffect(() => {
    router.push({ query: { page: pages } })
  }, [])

  const handleLoadPrev = (e) => {
    e.preventDefault()
    const parsing = parseInt(router.query.page) !== 1 ? parseInt(router.query.page) - 1 : 1

    setPages(parsing)
    router.push({ query: { page: parsing } })
  }

  const handleLoadNext = (e) => {
    e.preventDefault()
    const parsing = parseInt(router.query.page) + 1

    setPages(parsing)
    router.push({ query: { page: parsing } })
  }

  return (
    <div>
      <Head>
        <title>HOME | Pokemon</title>
        <meta name="description" content="This page showing list pokemon" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <HomePage
        listData={listData}
        handleLoadPrev={handleLoadPrev}
        handleLoadNext={handleLoadNext}
        pages={pages}
      />
    </div>
  )
}

export async function getServerSideProps({ query }) {
  const pages = parseInt(query?.page) || 1
  const nextOffset = pages === 1 ? 0 : (pages - 1) * 30
  let listData = null

  try {
    const res = await fetch(`${BASE_URL}pokemon?offset=${nextOffset}&limit=30`)
    const data = await res.json()
    
    listData = data.results.map((item, index) => ({
      ...item,
      image: `${IMAGE_URL}${index + 1}.png`
    }))
  } catch (err) {
    listData = { error: { message: err.message } }
  }

  return { props: { listData } }
}

export default Home
