import React, { useEffect, useState } from 'react'
import Link from 'next/link'

const Home = ({ listData, handleLoadPrev, handleLoadNext, pages }) => {
  const [listPokemon, setListPokemon] = useState([])

  useEffect(() => {
    setListPokemon(listData)
  }, [listData])

  const handleSearch = (e) => {
    const nameSearch = e.target.value
    let searching = listData.filter(item => {
      let target = item.name.toLowerCase()
      return target.indexOf(nameSearch) > -1
    })

    setTimeout(() => {
      setListPokemon(searching)
    }, 700)
  }

  return (
    <div className="container my-4">
      <div className="row my-5">
        <div className="col-12 col-md-6">
          <h2>List Pokemon</h2>
        </div>
        <div className="col-12 col-md-6">
          <input
            type="text"
            className="form-control"
            placeholder="Search Pokemon"
            onChange={handleSearch}
          />
        </div>
      </div>
      <div className="row">
        {
          listPokemon.length > 0 ? (
            listPokemon.map(item => (
              <div
                key={Math.random()}
                className="col-12 col-md-4 mb-3"
              >
                <Link href={`/detail/${item.name}`}>
                  <div className="card mb-3">
                    <div className="row g-0">
                      <div className="col-md-4">
                        <img
                          src={item.image}
                          className="img-fluid rounded-start w-100"
                          alt={`Picture of ${item.name}`}
                        />
                      </div>
                      <div className="col-md-8">
                        <div className="card-body">
                          <h5 className="card-title fw-bolder mb-3">
                            Pokemon
                          </h5>
                          <div className='wrapper-desc'>
                            <div className="fw-semibold mb-1">
                              Name:
                            </div>
                            <p className="card-text">
                              { item.name }
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            ))
          ) : (
            <p className="text-center">No Data</p>
          )
        }
      </div>
      <div className="row">
        <div className='col-6 text-start'>
          <button
            className="btn btn-primary"
            onClick={handleLoadPrev}
            disabled={pages === 1}
          >
            Prev
          </button>
        </div>
        <div className='col-6 text-end'>
          <button
            className="btn btn-primary"
            onClick={handleLoadNext}
          >
            Next
          </button>
        </div>
      </div>
    </div>
  )
}

export default Home
