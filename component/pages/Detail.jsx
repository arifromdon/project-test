import Router from 'next/router'

const Detail = ({
  dataDetail: {
    name,
    base_experience,
    types,
    stats,
    image,
    abilities,
    error
  }
}) => (
  <div className="container my-4">
    <div className="row my-5">
      <div className="col-12 d-flex align-items-center justify-content-between">
        <h2 className='text-capitalize'>
          Detail of { name }
        </h2>
        <div
          className='cursor-pointer'
          onClick={() => Router.back()}
        >
          Go Back
        </div>
      </div>
    </div>
    <div className="row">
      <div className="col-12 col-md-4">
        <div className='wrapper-image'>
          <img
            src={image}
            className="img-fluid rounded-start w-100"
            alt={`Picture of ${name}`}
          />
        </div>
      </div>
      <div className="col-12 col-md-8">
        <div className="row">
          <div className="col-12 col-md-6 mb-3">
            <label className='fw-bold'>
              Name:
            </label>
            <div className='text-capitalize'>
              { name }
            </div>
          </div>
          <div className="col-12 col-md-6 mb-3">
            <label className='fw-bold d-block'>
              Abilities:
            </label>
            {
              abilities.map(item => (
                <div
                  key={Math.random()}
                  className="badge text-bg-primary me-2"
                >
                  { item.ability.name }
                </div>
              ))
            }
          </div>
          <div className="col-12 col-md-6 mb-3">
            <label className='fw-bold'>
              Base Experience:
            </label>
            <p>{ base_experience }</p>
          </div>
          <div className="col-12 col-md-6 mb-3">
            <label className='fw-bold d-block'>
              Types:
            </label>
            {
              types.map(item => (
                <span
                  key={Math.random()}
                  className="badge text-bg-primary me-2"
                >
                  { item.type.name }
                </span>
              ))
            }
          </div>
          <div className="col-12">
            <label className='fw-bold'>
              Stats:
            </label>
            <ul>
              {
                stats.map(item => (
                  <li key={Math.random()} className="m-2">
                    <span className="me-2">
                      Base Stat: { item.base_stat } |
                    </span>
                    <span className="me-2">
                      Effort: { item.effort } |
                    </span>
                    <span className="me-2">
                      Stat: { item.stat.name }
                    </span>
                  </li>
                ))
              }
            </ul>
          </div>
        </div>
      </div>
    </div>
    {
      error && (
        <div className="row my-5">
          <div className='col-12'>
            {error.message}
          </div>
        </div>
      )
    }
  </div>
)

export default Detail
